//
//  ViewController.swift
//  Sudoku
//
//  Created by Breno Pinheiro Aquino on 05/09/17.
//  Copyright © 2017 Breno Pinheiro Aquino. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var numberLabel1: [UILabel]!
    
    @IBOutlet var numberLabel2: [UILabel]!
    
    @IBOutlet var numberLabel3: [UILabel]!
    
    @IBOutlet var numberLabel4: [UILabel]!
    
    @IBOutlet var numberLabel5: [UILabel]!
    
    @IBOutlet var numberLabel6: [UILabel]!
    
    @IBOutlet var numberLabel7: [UILabel]!
    
    @IBOutlet var numberLabel8: [UILabel]!
    
    @IBOutlet var numberLabel9: [UILabel]!
    
    var totalNumbersLabel: [[UILabel]] = []
    
    var numberSudoku: [[Int]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initSudoku()
        
        self.setSudoku()
        
        self.resolver()
    }
    
    // MARK: - Set/Init Sudoku
    
    func initSudoku() {
        
        totalNumbersLabel.append(numberLabel1)
        totalNumbersLabel.append(numberLabel2)
        totalNumbersLabel.append(numberLabel3)
        totalNumbersLabel.append(numberLabel4)
        totalNumbersLabel.append(numberLabel5)
        totalNumbersLabel.append(numberLabel6)
        totalNumbersLabel.append(numberLabel7)
        totalNumbersLabel.append(numberLabel8)
        totalNumbersLabel.append(numberLabel9)
    }
    
    func setSudoku() {
        
        totalNumbersLabel[0][0].text = "5"
        totalNumbersLabel[0][1].text = "6"
        totalNumbersLabel[0][3].text = "8"
        totalNumbersLabel[0][4].text = "4"
        totalNumbersLabel[0][5].text = "7"
        
        totalNumbersLabel[1][0].text = "3"
        totalNumbersLabel[1][2].text = "9"
        totalNumbersLabel[1][6].text = "6"
        
        totalNumbersLabel[2][2].text = "8"
        
        totalNumbersLabel[3][1].text = "1"
        totalNumbersLabel[3][4].text = "8"
        totalNumbersLabel[3][7].text = "4"
        
        totalNumbersLabel[4][0].text = "7"
        totalNumbersLabel[4][1].text = "9"
        totalNumbersLabel[4][3].text = "6"
        totalNumbersLabel[4][5].text = "2"
        totalNumbersLabel[4][7].text = "1"
        totalNumbersLabel[4][8].text = "8"
        
        totalNumbersLabel[5][1].text = "5"
        totalNumbersLabel[5][4].text = "3"
        totalNumbersLabel[5][7].text = "9"
        
        totalNumbersLabel[6][6].text = "2"
        
        totalNumbersLabel[7][2].text = "6"
        totalNumbersLabel[7][6].text = "8"
        totalNumbersLabel[7][8].text = "7"
        
        totalNumbersLabel[8][3].text = "3"
        totalNumbersLabel[8][4].text = "1"
        totalNumbersLabel[8][5].text = "6"
        totalNumbersLabel[8][7].text = "5"
        totalNumbersLabel[8][8].text = "9"
        
        var x = 0
        
        for _ in 0..<9 {
            
            numberSudoku.append([Int]())
        }
        
        for coluna in totalNumbersLabel {
            
            for num in coluna {
                
                numberSudoku[x].append(Int(num.text!)!)
                
                if num.text == "0" {
                    
                    num.text = "-"
                }
            }
            
            x += 1
        }
    }
    
    // MARK: - Logica
    
    func resolver() {
        
        var numTentativas = 0
        
        var isResolvido = false
        
        while !isResolvido {
            
            isResolvido = true
            
            var valores: [Int] = []
            var randomValor = 0
            
            for x in 0...8 {
                
                for y in 0...8 {
                    
                    valores = self.getNumerosPossiveis(x: x, y: y)
                    
                    if valores.count > 0 && numberSudoku[x][y] == 0 {
                        
                        randomValor = Int(arc4random_uniform(UInt32(valores.count)))
                        
                        numberSudoku[x][y] = valores[randomValor]
                        
                    }
                }
            }
            
            for x in 0...8 {
                
                for y in 0...8 {
                    
                    if numberSudoku[x][y] == 0 {
                        
                        totalNumbersLabel[x][y].text = "-"
                        
                        isResolvido = false
                        
                    } else {
                        
                        totalNumbersLabel[x][y].text = "\(numberSudoku[x][y])"
                    }
                }
            }
            
            numTentativas += 1
            
            print("Numero Total de Tentativas: \(numTentativas)")
        }
    }
    
    func getNumerosPossiveis(x: Int, y: Int) -> [Int] {
        
        var totosNumberosRelacionados: [Int] = []
        var possiveisValores: [Int] = []
        
        // Coluna
        
        for i in 0..<9 {
            
            totosNumberosRelacionados.append(numberSudoku[x][i])
        }
        
        // Linha
        
        for i in 0..<9 {
            
            totosNumberosRelacionados.append(numberSudoku[i][y])
        }
        
        // Quadrado
        
        let inicioQuadradoX = (Int(x/3) * 3)
        let inicioQuadradoY = (Int(y/3) * 3)
        
        for xQuadrado in inicioQuadradoX...inicioQuadradoX+2 {
            
            for yQuadrado in inicioQuadradoY...inicioQuadradoY+2 {
                
                totosNumberosRelacionados.append(numberSudoku[xQuadrado][yQuadrado])
            }
        }
        
        // Numeros Possiveis
        
        for num in 1...9 {
            
            if !totosNumberosRelacionados.contains(num) {
                
                possiveisValores.append(num)
            }
        }
        
        return possiveisValores
    }
  
}

